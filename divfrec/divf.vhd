LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY divf IS
	GENERIC(FREC: INTEGER := 24999999); -- FREC = (CLK_MST/2*CLK) - 1
	-- SI CLK_MST = 50 MHz
	-- FREC = 24999999 >>> 1 Hz
	-- FREC = 2499999  >>> 10 Hz
	-- FREC = 249999   >>> 100 Hz
	-- FREC = 24999    >>> 1 kHz
	PORT(CLK_MST: IN STD_LOGIC; -- CLK MASTER
				CLK: BUFFER STD_LOGIC); -- CLK FINAL
END ENTITY;

ARCHITECTURE BEHAVIOR OF divf IS
SIGNAL AUX: INTEGER RANGE 0 TO FREC;
BEGIN 
	PROCESS(CLK_MST)
	BEGIN 
		IF RISING_EDGE (CLK_MST) THEN
			IF AUX = 0 THEN  -- IF AUX = FREC THEN
				CLK <= NOT CLK;
				AUX <= FREC; -- AUX <= 0;
			ELSE
				AUX <= AUX - 1; -- AUX <= AUX + 1;
			END IF;
		END IF;
	END PROCESS;
END BEHAVIOR;